module mellium.im/xmpp/examples

go 1.17

require (
	github.com/gdamore/tcell/v2 v2.5.0
	github.com/rivo/tview v0.0.0-20220307222120-9994674d60a8
	mellium.im/sasl v0.2.1
	mellium.im/xmlstream v0.15.4-0.20211023152852-0ca80a938137
	mellium.im/xmpp v0.21.1
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/sys v0.0.0-20220318055525-2edf467146b5 // indirect
	golang.org/x/term v0.0.0-20210220032956-6a3ed077a48d // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.0.0-20180917221912-90fa682c2a6e // indirect
	mellium.im/reader v0.1.0 // indirect
)
