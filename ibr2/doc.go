// Copyright 2020 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

//go:generate go run ../internal/genfeature

// Package ibr2 implements Extensible In-Band Registration.
//
// Deprecated: this package implements an old version of the ibr2 protocol and
// should not be used.
package ibr2 // import "mellium.im/xmpp/ibr2"
